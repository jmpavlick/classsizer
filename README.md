# README #

Throwaway code to grab data from USABMX.com to show class size heading into the Grands. Uses regex to parse HTML, which as we all know is an [unforgivable sin](http://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags/1732454#1732454).

### Contribution guidelines ###

* Make a pull request. ;)

### Who do I talk to? ###

* Fire a PM to [facebook.com/john.pavlick](facebook.com/john.pavlick), or email jmpavlick@gmail.com.