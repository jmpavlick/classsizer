﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace ClassSizer
{
	class RiderClass
	{
		const string _URLFORMAT = @"http://usabmx.com/site/bmx_points?points_type=N.A.G.&section_id=46&age_group_id={0}&commit=View+Points&page={1}";

		public string AgeGroupId { get; set; }
		public string ClassTitle { get; set; }
		public int TotalRiders { get; set; }

		public string GetURL(int page)
		{
			return string.Format(_URLFORMAT, AgeGroupId, page);
		}

		public override string ToString()
		{
			return "Class Title: " + this.ClassTitle + "\tTotal Riders: " + this.TotalRiders;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			const string _LASTPAGEPATTERN = @"<liclass=""nextunavailable"">";
			const string _SINGLEPAGEPATTERN = @"<liclass=""next"">";
			const string _RANKPATTERN = @"\d+";
			const string _RIDERRANKPATTERN = @"<tr><td>" + _RANKPATTERN + "</td>";
			List<RiderClass> riders = new List<RiderClass>();

			foreach (string line in File.ReadAllLines(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ClassData.txt")))
			{
				RiderClass rc = new RiderClass { AgeGroupId = line.Split('\t')[0], ClassTitle = line.Split('\t')[1] };
				Console.WriteLine(rc.ClassTitle);

				string lastPageData;

				for (int i = 1; ; i++)
				{
					Console.WriteLine(i);
					using (var wc = new WebClient())
					{
						lastPageData = Regex.Replace(wc.DownloadString(rc.GetURL(i)), @"\s+", "");
					}

					// if lastPageData contains the "nextunavailable" class, it's the last page of a series
					// if lastPageData DOESN'T contain the "next" class, it's a single page series
					if (lastPageData.Contains(_LASTPAGEPATTERN) || lastPageData.Contains(_SINGLEPAGEPATTERN) == false)
					{
						break;
					}
				}

				// get total rider count
				int riderCount;
				string riderCountString = "";
				foreach (Match match in Regex.Matches(lastPageData, _RIDERRANKPATTERN))
				{
					riderCountString = Regex.Match(match.Value, _RANKPATTERN).Value;
				}

				Int32.TryParse(riderCountString, out riderCount);

				rc.TotalRiders = riderCount;
				riders.Add(rc);
			}

			// output the results
			using (StreamWriter sw = new StreamWriter(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "ClassSizerOutput.txt")))
			{
				foreach (var rc in riders)
				{
					sw.WriteLine(rc.ToString());
				}
			}
		}
	}
}
